# Google I/O 2017 Android Auto Messaging Codelab

This repository contains the source material for the Android Device Messaging codelab released at Google I/O 2017.

https://codelabs.developers.google.com/codelabs/device-messaging/index.html
